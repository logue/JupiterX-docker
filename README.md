# Jupiter Docker

これは、Minecraft PE向けのサーバープログラムであるNukkitの派生版のJupiterXのDocker Imageのリポジトリです。

<https://hub.docker.com/r/logue/JupiterX-docker/>

[![CircleCI](https://circleci.com/gh/JupiterDevelopmentTeam/JupiterX/tree/master.svg?style=svg)](https://circleci.com/gh/JupiterDevelopmentTeam/JupiterX/tree/master)
![Version](https://gitlab.com/logue/jupiterx-docker/raw/badges/version.png)
![Protocol](https://gitlab.com/logue/jupiterx-docker/raw/badges/protocol.png)

## このイメージの使い方

```sh
docker run -it -p 19132:19132/udp logue/JupiterX-docker
```

## ボリューム

このサーバーのワーキングディレクトリは /srv/JupiterX にマウントされ、設定ファイルや生成されたワールドなどのファイルシステムがマウントされます。

```sh
 docker run -v /home/[username]/JupiterX:/srv/JupiterX -p 19132:19132/udp logue/JupiterX-docker
```

## Dockerfile

DockerfileはGitLabにホストされています. [GitHubのJupiterXのリポジトリ](https://github.com/JupiterDevelopmentTeam/JupiterX)はGitLabにミラーしています。 
[ミラーされたリポジトリ](https://gitlab.com/logue/JupiterX)は、Jupiterリポジトリ更新時に自動的に同期され、Dockerイメージとともにリビルドされます。

## タグ

プロトコル番号を明示的に指定したい場合は、以下のようにコマンドを実行します。

```sh
docker pull logue/jupiterx-docker:83
```

Minecraft PEのバージョンのように指定することもできます。

```sh
logue/jupiterx-docker:0.15.6
```

## 問題点

Dockerイメージに関する問題などがありましたら[GitLab issue](https://gitlab.com/logue/JupiterX-docker/issues)まで報告お願いします。

## Contributing

You are invited to contribute new features, fixes, or updates, large or small; we are always thrilled to receive pull requests, and do our best to process them as fast as we can.

Before you start to code, we recommend discussing your plans through a [GitLab issue](https://gitlab.com/logue/jupiter-docker/issues), especially for more ambitious contributions. This gives other contributors a chance to point you in the right direction, give you feedback on your design, and help you find out if someone else is working on the same thing.