# Ubuntuをベースにする
FROM ubuntu:18.04

## 作成者
LABEL Logue <logue@hotmail.co.jp>

## APTキャッシュを更新
RUN apt-get update
RUN apt-get upgrade -y

## 日本語環境設定
RUN apt-get install -y language-pack-ja
RUN update-locale LANG=ja_JP.UTF-8 LANGUAGE=ja_JP:ja
ENV LANG ja_JP.UTF-8
ENV LC_ALL ja_JP.UTF-8
ENV LC_CTYPE ja_JP.UTF-8

## ロケールを基本日本語に設定
#RUN echo 'LC_ALL=ja_JP.UTF-8' > /etc/default/locale && echo 'LANG=ja_JP.UTF-8' >> /etc/default/locale
#RUN locale-gen ja_JP.UTF-8

## 時刻設定
RUN ln -fs /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

## ディスプレイ設定
#ENV DISPLAY :0.0

## Oracle Javaをインストール (ライセンス自動同意)
#RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
#RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" > /etc/apt/sources.list.d/webupd8team-java-xenial.list
#RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
#RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends oracle-java8-installer

## OpenJREをインストール
RUN apt-get install -y openjdk-8-jre-headless

## 依存ライブラリ
RUN apt-get install -y libxtst6

## 更新キャッシュをクリア
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

## Javaの環境設定
#ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV JAVA_OPTS -Djava.awt.headless=true

## マウント先
VOLUME /srv/JupiterX

## 作業ディレクトリ
WORKDIR /srv/JupiterX

## ポート番号
EXPOSE 19132

## ビルドしたjupiterをルートに移動する
COPY JupiterX/target/JupiterX-Build-1.0.jar /

## Jupiterを実行
CMD ["java", "-jar", "/JupiterX-Build-1.0.jar"]

