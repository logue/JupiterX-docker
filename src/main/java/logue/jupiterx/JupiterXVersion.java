package logue.jupiterx;

import blue.jupiterx.JupiterX;

public class JupiterXVersion {
  public static void main(String[] args) {
    System.out.print(JupiterX.JUPITERX_VERSION);
  }
}